----------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified. --
----------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
	mote_include_version = 2

	-- Load and initialize the include file.
	include('Mote-Include.lua')
end

-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()
	-- Unsure if this is needed at the moment.
	state.Buff['Sneak Attack'] = buffactive['sneak attack'] or false
	state.Buff['Trick Attack'] = buffactive['trick attack'] or false
	state.Buff['Feint'] = buffactive['feint'] or false

	-- These and th_action_check() may be deleteable.
	-- Not entirely valid on Era but needed to make the code work.
	-- For th_action_check():
	-- JA IDs for actions that always have TH: Provoke, Animated Flourish
	info.default_ja_ids = S{35, 204}
	-- Unblinkable JA IDs for actions that always have TH: Quick/Box/Stutter Step, Desperate/Violent Flourish
	info.default_u_ja_ids = S{201, 202, 203, 205, 207}

	include('Mote-TreasureHunter')
end

-----------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file. --
-----------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.  Can override this function in a sidecar file.
function user_setup()
	-- Your single weapon sets though note that 'Thief' is assumed to exist later in this gearswap.
	state.Weapon = M ('Dagger', 'Thief')

	-- Your dual weapon sets though note that 'Thief' is assumed to exist later in this gearswap.
	state.Weapons = M ('Dagger', 'Thief')

	-- Your ranged/ammo weapon sets and set to 'None' if you want no ranged weapons sets during //savetp.
	state.Ammo = M ('Bloody', 'Acid', 'Venom', 'Sleep', 'Blind', 'Paralyze', 'Silence', 'Asleep', 'Demon', 'None', 'Ungur')

	-- F9 cycles thru offense modes.
	state.OffenseMode:options('Haste', 'Accuracy', 'Evasion')

	-- Alt+F9 cycles ranged modes.
	state.RangedMode:options('Normal', 'Accuracy')

	-- Ctrl+F9 cycles hybrid modes.
	state.HybridMode:options('None')

	-- Doesn't have a default keybind to cycle modes.
	state.WeaponskillMode:options('Normal', 'Accuracy')

	-- Ctrl+F11 cycles casting modes.
	state.CastingMode:options('Normal')

	-- Ctrl+F12 cycles idle modes.
	state.IdleMode:options('Evasion')

	-- Doesn't have a default keybind to cycle modes.
	state.RestingMode:options('Normal')

	-- F10 to turn on PDT to your current PDT mode and Ctrl+F10 to turn off.
	state.PhysicalDefenseMode:options('PDT', 'None')

	-- F11 to turn on MDT to your current MDT mode and Ctrl+F10 to turn off.
	state.MagicalDefenseMode:options('MDT', 'None')

	-- Quick pin sets for the quick pin command.
	state.Pin = M ('None', 'Whistle', 'Warp', 'Nexus')

	-- Set true to false if you want to begin without TP saving on.
	state.SaveTP = M (true, 'Saving')

	-- Treasure hunter modes.
	state.TreasureMode:options('Tag', 'Fulltime', 'None')

	-- Aliases to control modes which I find much easier to use then remembering all those damn function keybinds.
	send_command('alias weapon gs c set Weapon')           -- //weapon set.name.here to change weapon sets.
	send_command('alias weapons gs c set Weapons')         -- //weapons set.name.here to change weapon sets.
	send_command('alias ammo gs c set Ammo')               -- //ammo set.name.here to change default ranged and ammo weapon sets.
	send_command('alias tp gs c set OffenseMode')          -- //tp set.name.here to change engaged sets.
	send_command('alias ranged gs c set RangedMode')       -- //range set.name.here to change ranged sets.
	send_command('alias hybrid gs c set HybridMode')       -- //hybrid set.name.here to change hybrid sets.
	send_command('alias ws gs c set WeaponskillMode')      -- //ws set.name.here to change ws mode sets.
	send_command('alias cast gs c set CastingMode')        -- //cast set.name.here to change casting mode sets.
	send_command('alias idle gs c set IdleMode')           -- //idle set.name.here to change idle mode sets.
	send_command('alias rest gs c set RestingMode')        -- //rest set.name.here to change resting mode sets.
	send_command('alias defense gs c set DefenseMode')     -- //defense 'physical' or 'magical' or 'none' may be required to even use PDT and MDT sets.
	send_command('alias pdt gs c set PhysicalDefenseMode') -- //pdt set.name.here to change PDT mode sets.
	send_command('alias mdt gs c set MagicalDefenseMode')  -- //mdt set.name.here to change MDT mode sets.
	send_command('alias kiting gs c toggle Kiting')        -- //kiting to toggle pinning the kiting set on or off.
	send_command('alias equip gs equip')                   -- //equip set.name.here to force equip a set but it does not lock it in place.
	send_command('alias pin gs c set Pin')                 -- //pin to quick equip and pin a set.
	send_command('alias savetp gs c toggle savetp')        -- //savetp to toggle your savetp state.
	send_command('alias th gs c set TreasureMode')         -- //th mode.name.here to set your TH mode.

	send_command('gs enable all')                   -- Unlock any slots that may have been locked before this gearswap was loaded.
	send_command('wait 10; input /lockstyleset 46') -- Sets your lockstyle set to the two digit number at the end.
	select_default_macro_book()                     -- Check the function at the bottom of this file to set the correct book and page number.

	send_command('bind %` gs c cycle treasuremode')        -- Press ` to cycle treasure hunter modes.
	send_command('bind %^space gs disable main sub range') -- Press ctrl + space to lock weapons.
	send_command('bind %!space gs enable main sub range')  -- Press alt + space to unlock weapons.

	dual_wield_check() -- Checks if you can dual wield and sets the var dualwield to true or false.

end

-- Called when this job file is unloaded (eg: job change)
function user_unload()
	-- Remove the bindings set above when this gearswap is unloaded.
	send_command('unbind %^space')
	send_command('unbind %!space')

--	send_command('clearaliases') -- That I know of there is no command to clear individual aliases and I don't want to stomp on anything.

end

---------------
-- Gear sets --
---------------

function init_gear_sets()

	sets.weapon  = {} -- Creates a empty list for single weapon sets to belong to.
	sets.weapons = {} -- Creates a empty list for dual weapon sets to belong to.
	sets.ammo    = {} -- Creates a emtpy list for ranged weapon sets to belong to.
	sets.pin     = {} -- Creates a empty list for pin sets to belong to.

	-----------------
	-- Global sets --
	-----------------

	sets.TreasureHunter = {hands = "Assassin's Armlets"}

	-----------------
	-- Weapon sets --
	-----------------

	sets.weapon.Dagger = {
		main = "Mandau",
		sub = "Master Shield",
	}

	sets.weapon.Thief  = {
		main = "Thief's Knife",
		sub  = "Shield",
	}

	sets.weapons.Dagger = {
		main = "Mandau",
		sub  = "Kraken Club",
	}

	sets.weapons.Thief = {
		main = "Mandau",
		sub  = "Thief's Knife",
	}

	---------------------------------
	-- Ranged weapon and ammo sets --
	---------------------------------

	-- Just put your range and ammo pieces in here.
	sets.ammo.Crossbow = {range = "Staurobow"}    -- Used by all your bolt sets but not actually a true ammo set itself.
	sets.ammo.Bow      = {range = "Lamian Kaman"} -- Used by all your arrow sets but not actually a true ammo set itself.

	sets.ammo.Bloody   = set_combine(sets.ammo.Crossbow, {ammo = "Bloody Bolt"})
	sets.ammo.Acid     = set_combine(sets.ammo.Crossbow, {ammo = "Acid Bolt"})
	sets.ammo.Venom    = set_combine(sets.ammo.Crossbow, {ammo = "Venom Bolt"})
	sets.ammo.Sleep    = set_combine(sets.ammo.Crossbow, {ammo = "Sleep Bolt"})
	sets.ammo.Blind    = set_combine(sets.ammo.Crossbow, {ammo = "Blind Bolt"})
	sets.ammo.Paralyze = set_combine(sets.ammo.Bow, {ammo = "Paralysis Arrow"})
	sets.ammo.Silence  = set_combine(sets.ammo.Bow, {ammo = "Kabura Arrow"})
	sets.ammo.Asleep   = set_combine(sets.ammo.Bow, {ammo = "Sleep Arrow"})
	sets.ammo.Demon    = set_combine(sets.ammo.Bow, {ammo = "Demon Arrow"})

	sets.ammo.Ungur    = {range = "Ungur Boomerang", ammo={""}}

	---------------
	-- Idle sets --
	---------------

	sets.idle.Evasion = {
		ammo  = "Fenrir's Stone", -- Mainly because of Fire Bomblet a Evasion (sometimes) ammo that can swap without losing tp.
		head  = "Empress Hairpin",
		neck  = "Orochi Nodowa",
		ear1  = "Drone Earring",
		ring1 = "Breeze Ring",
		ear2  = "Drone Earring",
		ring2 = "Breeze Ring",
		body  = "Antares Harness",
		hands = "War Gloves +1",
		back  = "Boxer's Mantle",
		waist = "Scouter's Rope",
		legs  = "Rogue's Culottes",
		feet  = "Trotter Boots",
	}

--	sets.idle.Town = {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

--	sets.idle.Weak = {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

	sets.resting = {
		main  = " ",
		sub   = " ",
		range = " ",
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}
	
	------------------
	-- Engaged sets --
	------------------

	-- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
	-- sets if more refined versions aren't defined.
	-- If you create a set with both offense and defense modes, the offense mode should be first.
	-- EG: sets.engaged.Dagger.Accuracy.Evasion
	
	sets.engaged.Haste = {
		ammo  = "Tiphia Sting",
		head  = "Walahra Turban",
		neck  = "Tiercel Necklace",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Ecphoria Ring",
		body  = "Rapparee Harness",
		hands = "Dusk Gloves +1",
		back  = "Cuchulain's Mantle",
		waist = "Velocious Belt",
		legs  = "Enkidu's Subligar",
		feet  = "Dusk Ledelsens +1",
	}

	sets.engaged.Accuracy = {
		ammo  = "Tiphia Sting",
		head  = "Optical Hat",
		neck  = "Chivalrous Chain",
		ear1  = "Brutal Earring",
		ring1 = "Lava's Ring",
		ear2  = "Suppanomimi",
		ring2 = "Kusha's Ring",
		body  = "Homam Corazza",
		hands = "War Gloves +1",
		back  = "Cuchulain's Mantle",
		waist = "Virtuoso Belt",
		legs  = "Oily Trousers",
		feet  = "Dusk Ledelsens +1",
	}

	sets.engaged.Evasion = {
		ammo  = "Tiphia Sting",
		head  = "Optical Hat",
		neck  = "Evasion Torque",
		ear1  = "Drone Earring",
		ring1 = "Breeze Ring",
		ear2  = "Suppanomimi",
		ring2 = "Breeze Ring",
		body  = "Antares Harness",
		hands = "War Gloves +1",
		back  = "Boxer's Mantle",
		waist = "Scouter's Rope",
		legs  = "Oily Trousers",
		feet  = "Emissary Boots",
	}

	----------------
	-- Range sets --
	----------------

	-- This might benefit from all your haste gear not on Era though.
	sets.precast.RA = {
		main = "Velocity Bow",
		sub  = "Bloody Bolt",
	}

	-- Put your Ranged attack enhancing gear here.
	sets.midcast.RA = {
		range = "Staurobow",
		ammo  = "Bloody Bolt",
		head  = "Hecatomb Cap",
		neck  = "Peacock Amulet",
		ear1  = "Drone Earring",
		ring1 = "Rajas Ring",
		ear2  = "Drone Earring",
		ring2 = "Flame Ring",
		body  = "Hecatomb Harness",
		hands = "Hecatomb Mittens",
		back  = "Cerberus Mantle",
		legs  = "Dusk Trousers",
		feet  = "Hecatomb Leggings",
	}

	-- But your Ranged accuracy and attack enhancing gear here.
	sets.midcast.RA.Accuracy = set_combine(sets.midcast.RA, {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	})

	-----------------------
	-- Weapon skill sets --
	-----------------------

		---------------
		-- Universal --
		---------------

	-- Default WS set.
	sets.precast.WS = {
		ammo  = "Tiphia Sting",
		head  = "Hecatomb Cap",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Coral Earring",
		ring2 = "Flame Ring",
		body  = "Hecatomb Harness",
		hands = "Hecatomb Mittens",
		back  = "Cerberus Mantle",
		waist = "Warwolf Belt",
		legs  = "Hecatomb Subligar",
		feet  = "Hecatomb Leggings",
	}

	-- Default WS set (Accuracy mode).
	sets.precast.WS.Accuracy = {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	}

	-- For any WS that doesn't have a set used while SA is active.
	sets.precast.WS.SA = {
		head  = "Hecatomb Cap",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Pixie Earring",
		ring2 = "Thunder Ring",
		body  = "Hecatomb Harness",
		hands = "Hecatomb Mittens",
		back  = "Cuchulain's Mantle",
		waist = "Warwolf Belt",
		legs  = "Hecatomb Subligar",
		feet  = "Hecatomb Leggings",
	}

	-- For any WS that doesn't have a set used while TA is active.
	sets.precast.WS.TA = {
		head  = "Dragon Cap",
		neck  = "Fotia Gorget",
		ear1  = "Drone Earring",
		ring1 = "Breeze Ring",
		ear2  = "Drone Earring",
		ring2 = "Breeze Ring",
		body  = "Hecatomb Harness",
		hands = "Dragon Mittens",
		back  = "Assassin's Cape",
		waist = "Scouter's Rope",
		legs  = "Oily Trousers",
		feet  = "Bounding Boots",
	}

	-- For any WS that doesn't have a set used while SATA is active.
	sets.precast.WS.SATA = {
		head  = "Hecatomb Cap",
		neck  = "Fotia Gorget",
		ear1  = "Drone Earring",
		ring1 = "Rajas Ring",
		ear2  = "Pixie Earring",
		ring2 = "Thunder Ring",
		body  = "Hecatomb Harness",
		hands = "Hecatomb Mittens",
		back  = "Cuchulain's Mantle",
		waist = "Warwolf Belt",
		legs  = "Hecatomb Subligar",
		feet  = "Hecatomb Leggings",
	}

		-----------------
		-- Exenterator --
		-----------------

	-- The secondary stat scaling on this WS seems to be working, I built a heavy dex set but should probable be chr.
	sets.precast.WS['Exenterator'] = {
		head  = "Hecatomb Cap",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Pixie Earring",
		ring2 = "Thunder Ring",
		body  = "Dragon Harness",
		hands = "Hecatomb Mittens",
		back  = "Cuchulain's Mantle",
		waist = "Warwolf Belt",
		legs  = "Hecatomb Subligar",
		feet  = "Hecatomb Leggings",
	}

	sets.precast.WS['Exenterator'].accuracy = {
--		head  = "Hecatomb Cap",
--		neck  = "Fotia Gorget",
--		ear1  = "Brutal Earring",
--		ring1 = "Rajas Ring",
--		ear2  = "Pixie Earring",
--		ring2 = "Thunder Ring",
--		body  = "Dragon Harness",
--		hands = "Hecatomb Mittens",
--		back  = "Cuchulain's Mantle",
--		waist = "Warwolf Belt",
--		legs  = "Hecatomb Subligar",
--		feet  = "Hecatomb Leggings",
	}

	-- Forces this WS to use the universal SA, TA, and SATA WS sets during those buffs.
	sets.precast.WS['Exenterator'].SA = sets.precast.WS.SA
	sets.precast.WS['Exenterator'].TA = sets.precast.WS.TA
	sets.precast.WS['Exenterator'].SATA = sets.precast.WS.SATA

		------------------
		-- Mercy Stroke --
		------------------

	sets.precast.WS['Mercy Stroke'] = {
		ammo  = "Peiste Dart",
		head  = "Hecatomb Cap",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Coral Earring",
		ring2 = "Flame Ring",
		body  = "Hecatomb Harness",
		hands = "Alkyoneus's Bracelets",
		back  = "Cerberus Mantle",
		waist = "Warwolf Belt",
		legs  = "Hecatomb Subligar",
		feet  = "Hecatomb Leggings",
	}

	sets.precast.WS['Mercy Stroke'].Accuracy = {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	}

	-- Forces this WS to use the universal SA, TA, and SATA WS sets during those buffs.
	sets.precast.WS['Mercy Stroke'].SA = sets.precast.WS.SA
	sets.precast.WS['Mercy Stroke'].TA = sets.precast.WS.TA
	sets.precast.WS['Mercy Stroke'].SATA = sets.precast.WS.SATA

	-----------------------
	-- Job ability sets. --
	-----------------------

	-- Testing Accuracy for Steps.
	sets.precast.Step = {
		ammo  = "Tiphia Sting",
		head  = "Optical Hat",
		neck  = "Peacock Amulet",
--		ear1  = "Brutal Earring",
		ring1 = "Lava's Ring",
--		ear2  = "Suppanomimi",
		ring2 = "Kusha's Ring",
		body  = "Homam Corazza",
		hands = "War Gloves +1",
		back  = "Cuchulain's Mantle",
		waist = "Virtuoso Belt",
		legs  = "Oily Trousers",
--		feet  = "Dusk Ledelsens +1",
	}

	sets.precast.JA['Steal'] = {
		head  = "Rogue's Bonnet",
		neck  = "Rabbit Charm",
		hands = "Rogue's Armlets",
		legs  = "Assassin's Culottes",
		feet  = "Rogue's Poulaines",
	}

	sets.precast.JA['Mug']  = {head="Assassin's Bonnet"}
	sets.precast.JA["Hide"] = {body="Rogue's Vest"}
	sets.precast.JA["Flee"] = {feet="Rogue's Poulaines"}

	sets.precast.JA['Collaborator'] = {}
	sets.precast.JA['Accomplice']   = {}

	sets.buff['Sneak Attack'] = {
		head  = "Hecatomb Cap",
		neck  = "Spike Necklace",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Pixie Earring",
		ring2 = "Thunder Ring",
		body  = "Dragon Harness",
		hands = "Hecatomb Mittens",
		back  = "Cuchulain's Mantle",
		waist = "Warwolf Belt",
		legs  = "Hecatomb Subligar",
		feet  = "Hecatomb Leggings",
	}

	sets.buff['Trick Attack'] = {
		head  = "Dragon Cap",
		neck  = "Orochi Nodowa",
		ear1  = "Drone Earring",
		ring1 = "Breeze Ring",
		ear2  = "Drone Earring",
		ring2 = "Breeze Ring",
		body  = "Dragon Harness",
		hands = "Dragon Mittens",
		back  = "Assassin's Cape",
		waist = "Scouter's Rope",
		legs  = "Oily Trousers",
		feet  = "Bounding Boots",
	}

--	sets.precast.Waltz = {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}
		
--	sets.precast.Waltz['Healing Waltz'] = {} -- Ensures that Healing Waltz doesn't swap gear in. It does not benefit from anything.

	------------------
	-- Midcast sets --
	------------------

	sets.precast.FC = {ear1="Loquacious Earring"} -- Sets for fast cast gear for spells

	-- Generic spell recast set (Haste > Interrupt > Evasion > PDT) and the default casting set if you have none other.
	sets.midcast.FastRecast = {
		head  = "Walahra Turban",
		neck  = "Tiercel Necklace",
		body  = "Rapparee Harness",
		hands = "Dusk Gloves +1",
		waist = "Velocious Belt",
		legs  = "Bravo's Subligar",
		feet  = "Dusk Ledelsens +1",
	}

	sets.midcast.Utsusemi = sets.midcast.FastRecast -- To be honest not really needed but just in case.

	------------------
	-- Defense sets --
	------------------

	-- To use these you must set (//defense magical_or_physical) in conjuction with
	-- (//pdt set_name) or (//mdt set_name) -.- not a fan of how he did these.
	-- Tri-mode defense sets that all share a name space </3.

	sets.defense.PDT = {}

	sets.defense.MDT = {}

	sets.Kiting = {feet = "Trotter Boots"} -- Alt+F10 to toggle pinning on and off.

	--------------
	-- Pin sets --
	--------------

	--[[
	These sets can be quick pinned with //pin set_name_here
	They can also be unpinned with //pin none
	But only one set can be pinned at a time that way.

	These sets can also be called and pinned in a windower macro using the example script.txt below.
	gs enable neck
	gs equip manual.whistle
	gs disable neck

	Or used (and pinned) in a standard in game macro with the example below.
	/console gs enable neck
	/console equip pin.whistle
	/console gs disable neck

	Make sure you enable then equip and then disable all the slots that are used in the sets.
	--]]

	sets.pin.Whistle = {neck  = "Chocobo Whistle"}
	sets.pin.Warp    = {ring2 = "Warp Ring"}
	sets.pin.Nexus   = {back  = "Nexus Cape"}

	----------------------
	-- Custom buff sets --
	----------------------

	sets.buff.Sleep = {neck = "Opo-opo Necklace"} -- Need to code this.

end

-----------------------------------------------------
-- Job-specific hooks for standard casting events. --
-----------------------------------------------------

-- Set eventArgs.handled to true if we don't want any automatic target handling to be done.
function job_pretarget(spell, action, spellMap, eventArgs)

end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
-- Set eventArgs.useMidcastGear to true if we want midcast gear equipped on precast.
function job_precast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default precast() is done.
-- eventArgs is the same one used in job_precast, in case information needs to be persisted.
function job_post_precast(spell, action, spellMap, eventArgs)
--[[
	-- Uses your treasure hunter gear when you use a AOE WS if in a treasure hunter mode.
	if spell.english == 'Cyclone' or spell.english == 'Aeolian Edge' and state.TreasureMode.value ~= 'None' then
		equip(sets.TreasureHunter)
	end
--]]

	cast_override_slots()
end


-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)
--[[
	-- Uses your treasure hunter gear when you range attack if in a treasure hunter mode.
	if state.TreasureMode.value ~= 'None' and spell.action_type == 'Ranged Attack' then
		equip(sets.TreasureHunter)
	end
--]]

	cast_override_slots()
end

-- Runs when a pet initiates an action.
-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_pet_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default pet midcast() is done.
-- eventArgs is the same one used in job_pet_midcast, in case information needs to be persisted.
function job_pet_post_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_aftercast(spell, action, spellMap, eventArgs)
	-- If you WS with sneak or trick on it uses your WS gear without this and can't use custom wsmodes.
	if spell.type == 'WeaponSkill' and not spell.interrupted then
		state.Buff['Sneak Attack'] = false
		state.Buff['Trick Attack'] = false
		state.Buff['Feint'] = false
	end

	cast_override_slots()
end

-- Run after the default aftercast() is done.
-- eventArgs is the same one used in job_aftercast, in case information needs to be persisted.
function job_post_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_pet_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default pet aftercast() is done.
-- eventArgs is the same one used in job_pet_aftercast, in case information needs to be persisted.
function job_pet_post_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Function to overide certain slots like weapons in different job event fuctions under different conditions.
function cast_override_slots()
	if state.SaveTP.value == true then
		equip(sets.ammo[state.Ammo.value])
		if state.TreasureMode.value == "Fulltime" then
			if dualwield == false then
				equip(sets.weapon['Thief'])
			else
				equip(sets.weapons['Thief'])
			end
		else
			if dualwield == false then
				equip(sets.weapon[state.Weapon.current])
			else
				equip(sets.weapons[state.Weapons.current])
			end
		end
	end
	equip(sets.pin[state.Pin.current])
end

------------------------------------------------
-- Job-specific hooks for non-casting events. --
------------------------------------------------

-- Called when the player's status changes.
function job_status_change(newStatus, oldStatus, eventArgs)

end

-- Called when the player's pet's status changes.
function job_pet_status_change(newStatus, oldStatus, eventArgs)

end

-- Called when a player gains or loses a buff.
-- buff == buff gained or lost
-- gain == true if the buff was gained, false if it was lost.
function job_buff_change(buff, gain)
-- Called when a player gains or loses a buff.
-- buff == buff gained or lost
-- gain == true if the buff was gained, false if it was lost.
-- Without this when buffs end the gear never changes back.
	if state.Buff[buff] ~= nil then
		if not midaction() then
			handle_equipping_gear(player.status)
		end
	end

end

-- Called when a generally-handled state value has been changed.
function job_state_change(stateField, newValue, oldValue)

end

------------------------------------------------------------
-- User code that supplements standard library decisions. --
------------------------------------------------------------

-- Called before the Include starts constructing melee/idle/resting sets.
-- Can customize state or custom melee class values at this point.
-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_handle_equipping_gear(status, eventArgs)
	-- Checks for the presence of certain jobability buffs.
	check_buff('Trick Attack', eventArgs)
	check_buff('Sneak Attack', eventArgs)

end

-- Custom spell mapping.
-- Return custom spellMap value that can override the default spell mapping.
-- Don't return anything to allow default spell mapping to be used.
function job_get_spell_map(spell, default_spell_map)

end

-- Return a customized weaponskill mode to use for weaponskill sets.
-- Don't return anything if you're not overriding the default value.
function get_custom_wsmode(spell, spellMap, default_wsmode)

	-- Makes it look for custom WS subsets with .SA and TA and .SATA for every WS set.
	local wsmode

	if state.Buff['Sneak Attack'] then
		wsmode = 'SA'
	end

	if state.Buff['Trick Attack'] then
		wsmode = (wsmode or '') .. 'TA'
	end

	return wsmode
end

-- Modify the default idle set after it was constructed.
function customize_idle_set(idleSet)
	if state.SaveTP.value == true then
		idleSet = override_slots(override_weapons(idleSet))
	else
		idleSet = override_slots(idleSet)
	end

	return idleSet
end

-- Modify the default melee set after it was constructed.
function customize_melee_set(meleeSet)
	meleeSet = override_slots(override_weapons(meleeSet))

	return meleeSet
end

-- Modify the default defense set after it was constructed.
function customize_defense_set(defenseSet)
	defenseSet = override_slots(override_weapons(defenseSet))

	return defenseSet
end

-- Called by the 'update' self-command, for common needs.
-- Set eventArgs.handled to true if we don't want automatic equipping of gear.
function job_update(cmdParams, eventArgs)
    th_update(cmdParams, eventArgs)
end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)

end

-- Separated from the general slot override part contains code to override weapons, ranged, and ammo.
function override_weapons(Set)
	if state.TreasureMode.value == "Fulltime" then
		if dualwield == false then
			Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapon['Thief'])
		else
			Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapons['Thief'])
		end
	else
		if dualwield == false then
			Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapon[state.Weapon.current])
		else
			Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapons[state.Weapons.current])
		end
	end

	return Set
end

-- Separated from the weapon override part contains code to override everything but weapons, ranged, and ammo.
function override_slots(Set)
	Set = set_combine(Set, sets.pin[state.Pin.current])

	return Set
end

-----------------------------------------------
-- User code that supplements self-commands. --
-----------------------------------------------

-- Called for custom player commands.
function job_self_command(cmdParams, eventArgs)

end

-- Job-specific toggles.
function job_toggle_state(field)

end

-- Request job-specific mode lists.
-- Return the list, and the current value for the requested field.
function job_get_option_modes(field)

end

-- Set job-specific mode values.
-- Return true if we recognize and set the requested field.
function job_set_option_mode(field, val)

end

-- Handle auto-targetting based on local setup.
function job_auto_change_target(spell, action, spellMap, eventArgs)

end

---------------------------------------------
-- Utility functions specific to this job. --
---------------------------------------------

-- Checks if you can dual wield or not and sets the correct default weapon set.
function dual_wield_check()
	if player.job ~= "NIN" or player.job ~= "DNC" then
		if player.sub_job == "NIN" and player.sub_job_level >= 10 or
		   player.sub_job == "DNC" and player.sub_job_level >= 20 then
			dualwield = true
		else
			dualwield = false
		end
	else
		dualwield = true
	end
end

function check_buff(buff_name, eventArgs)
	if state.Buff[buff_name] then
		equip(sets.buff[buff_name] or {})
		eventArgs.handled = true
	end
end

-- Required for Treasure Hunter Tag mode to know when certain things have put your TH level on.
-- Check for various actions that we've specified in user code as being used with TH gear.
-- This will only ever be called if TreasureMode is not 'None'.
-- Category and Param are as specified in the action event packet.
function th_action_check(category, param)
	if category == 2 or -- any ranged attack
		 category == 4 or -- any magic action
		(category == 3 and param == 30) or -- Aeolian Edge
		(category == 6 and info.default_ja_ids:contains(param)) or -- Provoke, Animated Flourish
		(category == 14 and info.default_u_ja_ids:contains(param)) -- Quick/Box/Stutter Step, Desperate/Violent Flourish
		then return true
	end
end

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
	-- Default macro set/book
		set_macro_page(1, 6)
end
