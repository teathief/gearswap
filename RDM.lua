----------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified. --
----------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
	mote_include_version = 2
	-- Shouldn't have to edit any of these sets (unless using NQ staffs on a serious budget) if you don't use them turn it off below.
	sets.staff = {
		Fire      = {main = "Vulcan's Staff"},
		Earth     = {main = "Terra's Staff"},
		Water     = {main = "Neptune's Staff"},
		Wind      = {main = "Auster's Staff"},
		Ice       = {main = "Aquilo's Staff"},
		Lightning = {main = "Jupiter's Staff"},
		Light     = {main = "Apollo's Staff"},
		Dark      = {main = "Pluto's Staff"},
	}

	sets.grips = {
		Fire      = {sub = "Fire Grip"},
		Earth     = {sub = "Earth Grip"},
		Water     = {sub = "Water Grip"},
		Wind      = {sub = "Wind Grip"},
		Ice       = {sub = "Ice Grip"},
		Lightning = {sub = "Thunder Grip"},
		Light     = {sub = "Light Grip"},
		Dark      = {sub = "Dark Grip"},
	}

	sets.obi = {
		Fire      = {waist = "Karin Obi"},
		Earth     = {waist = "Dorin Obi"},
		Water     = {waist = "Suirin Obi"},
		Wind      = {waist = "Furin Obi"},
		Ice       = {waist = "Hyorin Obi"},
		Lightning = {waist = "Rairin Obi"},
		Light     = {waist = "Korin Obi"},
		Dark      = {waist = "Anrin Obi"},
		Master    = {waist = "Hachirin-no-Obi"},
	}

	-- Load and initialize the include file.
	include('Mote-Include.lua')
end

-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()

end

-----------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file. --
-----------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.  Can override this function in a sidecar file.
function user_setup()

	-- Your single wielding weapon sets.
	state.Weapon = M ('Sword', 'Dagger', 'Kraken')

	-- Your dual wielding weapon sets.
	state.Weapons = M ('Sword' , 'Dagger', 'Excalibur', 'Mandau', 'Multi', 'Shield')

	-- Your ranged/ammo weapon sets and set to 'None' if you want no ranged weapons sets during //savetp.
	state.Ammo = M ('None', 'Normal')

	-- F9 cycles thru offense modes.
	state.OffenseMode:options('Normal', 'Haste', 'Accuracy', 'Hydra')

	-- Alt+F9 cycles ranged modes.
	state.RangedMode:options('Normal', 'Accuracy')

	-- Ctrl+F9 cycles hybrid modes.
	state.HybridMode:options('None', 'Refresh')

	-- Doesn't have a default keybind to cycle modes.
	state.WeaponskillMode:options('Normal', 'Accuracy')

	-- Ctrl+F11 cycles casting modes.
	state.CastingMode:options('Normal', 'Resist')

	-- Ctrl+F12 cycles idle modes.
	state.IdleMode:options('PDT', 'MDT')

	-- Doesn't have a default keybind to cycle modes.
	state.RestingMode:options('Normal', 'Zenith')

	-- F10 to turn on PDT to your current PDT mode and Ctrl+F10 to turn off.
	state.PhysicalDefenseMode:options('PDT', 'Refresh', 'None')

	-- F11 to turn on MDT to your current MDT mode and Ctrl+F10 to turn off.
	state.MagicalDefenseMode:options('MDT', 'Refresh', 'None')

	-- Quick pin sets for the quick pin command.
	state.Pin = M ('None', 'Whistle', 'Warp', 'Nexus', 'Lotus', 'Terra', 'Refresh', 'Zenith')

	-- Set true to false if you want to begin without TP saving on.
	state.SaveTP = M (false, 'Saving')

	-- //grip to toggle elemental grip usuage, and set true to false if you want to begin with Grip's off.
	state.Staff = M (false, "Elemental Staffs")
	IceStaff = true -- true or false toggles always using the ice staff with ice elemental magic even if staff is off.

	-- //grip to toggle elemental grip usuage, and set true to false if you want to begin with Grip's off.
	state.Grip = M (false, "Elemental Grips")

	-- //obi to toggle elemental obi usuage, and set true to false if you want to begin with Obi's off.
	state.Obi = M (true, "Elemental Obis")
	MasterObi = true -- This sets if you have the universal master elemental obi and uses that instead and if not set it to false.

-- Aliases to control modes which I find much easier to use then remembering all those damn function keybinds.
	send_command('alias weapon gs c set Weapon')           -- //weapon set.name.here to change single weapon sets.
	send_command('alias weapons gs c set Weapons')         -- //weapons set.name.here to change dual weapon sets.
	send_command('alias ammo gs c set Ammo')               -- //ammo set.name.here to change default ranged and ammo weapon sets.
	send_command('alias tp gs c set OffenseMode')          -- //tp set.name.here to change engaged sets.
	send_command('alias ranged gs c set RangedMode')       -- //range set.name.here to change ranged sets.
	send_command('alias hybrid gs c set HybridMode')       -- //hybrid set.name.here to change hybrid sets.
	send_command('alias ws gs c set WeaponskillMode')      -- //ws set.name.here to change ws mode sets.
	send_command('alias cast gs c set CastingMode')        -- //cast set.name.here to change casting mode sets.
	send_command('alias idle gs c set IdleMode')           -- //idle set.name.here to change idle mode sets.
	send_command('alias rest gs c set RestingMode')        -- //rest set.name.here to change resting mode sets.
	send_command('alias defense gs c set DefenseMode')     -- //defense 'physical' or 'magical' or 'none' may be required to even use PDT and MDT sets.
	send_command('alias pdt gs c set PhysicalDefenseMode') -- //pdt set.name.here to change PDT mode sets.
	send_command('alias mdt gs c set MagicalDefenseMode')  -- //mdt set.name.here to change MDT mode sets.
	send_command('alias kiting gs c toggle Kiting')        -- //kiting to toggle pinning the kiting set on or off.
	send_command('alias equip gs equip')                   -- //equip set.name.here to force equip a set but it does not lock it in place.
	send_command('alias pin gs c set Pin')                 -- //pin to quick equip and pin a set.
	send_command('alias savetp gs c toggle savetp')        -- //savetp to toggle your savetp state.
	send_command('alias staff gs c toggle Staff')          -- //staff to toggle using elemental staffs.
	send_command('alias grip gs c toggle Grip')            -- //grip to toggle using elemental grips.
	send_command('alias obi gs c toggle Obi')              -- //obi to toggle using elemental obis.

	send_command('gs enable all')                   -- Unlock any slots that may have been locked before this gearswap was loaded.
	send_command('wait 10; input /lockstyleset 85') -- Sets your lockstyle set to the two digit number at the end.
	select_default_macro_book()                     -- Check the function at the bottom of this file to set the correct book and page number.

	send_command('bind %^space gs disable main sub range') -- Press ctrl + space to lock weapons.
	send_command('bind %!space gs enable main sub range')  -- Press alt + space to unlock weapons.

	dual_wield_check() -- Checks if you can dual wield and sets the var dualwield to true or false.

end

-- Called when this job file is unloaded (eg: job change)
function user_unload()
	-- Remove the bindings set above when this gearswap is unloaded.
	send_command('unbind %^space')
	send_command('unbind %!space')

--	send_command('clearaliases') -- That I know of there is no command to clear individual aliases and I don't want to stomp on anything.

end

---------------
-- Gear sets --
---------------

function init_gear_sets()

	sets.weapon  = {} -- Creates a empty list for single weapon sets to belong to.
	sets.weapons = {} -- Creates a empty list for dual weapon sets to belong to.
	sets.ammo    = {} -- Creates a emtpy list for ranged weapon sets to belong to.
	sets.pin     = {} -- Creates a empty list for pin sets to belong to.

	-----------------
	-- Global sets --
	-----------------

	-- Refresh gear used in some sets and modes.
	sets.Refresh = {
		head = "Duelist's Chapeau +1",
		body = "Dalmatica",
	}

	-- MP whore set.
	sets.Zenith  = {
		head  = "Zenith Crown",
		body  = "Dalmatica",
		hands = "Zenith Mitts +1",
--		ring2 = "Vivian Ring",
		legs  = "Zenith Slacks",
		feet  = "Zenith Pumps",
	}

	-----------------
	-- Weapon sets --
	-----------------

	sets.weapon.Sword = {
		main = "Excalibur",
		sub  = "Genbu's Shield",
	}

	sets.weapon.Dagger = {
		main = "Mandau",
		sub  = "Genbu's Shield",
	}

	-- For fucking around with /RNG.
	sets.weapon.Kraken = {
		main = "Kraken Club",
		sub  = "Genbu's Shield",
	}

	sets.weapons.Sword = {
		main = "Excalibur",
		sub  = "Kraken Club",
	}

	sets.weapons.Dagger = {
		main = "Mandau",
		sub  = "Kraken Club",
	}

	sets.weapons.Murgleis = {
		main = "Murgleis",
		sub  = "Kraken Club",
	}

	sets.weapons.Excalibur = {
		main = "Mandau",
		sub  = "Excalibur",
	}

	sets.weapons.Mandau = {
		main = "Excalibur",
		sub  = "Mandau",
	}

	sets.weapons.Multi = {
		main = "Joyeuse",
		sub  = "Justice Sword",
		ammo = "Virtue Stone",
	}

	sets.weapons.Shield = {
		main = "Excalibur",
		sub  = "Genbu's Shield",
	}

	---------------------------------
	-- Ranged weapon and ammo sets --
	---------------------------------

	-- Just put your range and ammo pieces in here.
	sets.ammo.Normal = {
		range = "Failnaught",
		ammo  = "Kabura Arrow",
	}

	---------------
	-- Idle sets --
	---------------

	sets.idle.PDT = {
		main  = "Terra's Staff",
		sub   = "Raptor Leather Strap +1",
		ammo  = "Bibiki Seashell",
		head  = "Duelist's Chapeau +1",
		neck  = "Orochi Nodowa",
		ear1  = "Star Earring",
		ring1 = "Star Ring",
		ear2  = "Star Earring",
		ring2 = "Star Ring",
		body  = "Dalmatica",
		hands = "Duelist's Gloves +1",
		back  = "Umbra Cape",
		waist = "Steppe Sash",
		legs  = "Crimson Cuisses",
		feet  = "Crimson Greaves"
	}

	sets.idle.MDT = {
		main  = "Chatoyant Staff",
		sub   = "Raptor Leather Strap +1",
		range = "Lamian Kaman",
		ammo  = "Kabura Arrow",
		head  = "Duelist's Chapeau +1",
		neck  = "Jeweled Collar",
		ear1  = "Star Earring",
		ring1 = "Star Ring",
		ear2  = "Star Earring",
		ring2 = "Star Ring",
		body  = "Dalmatica",
		hands = "Duelist's Gloves +1",
		back  = "Hexerei Cape",
		waist = "Steppe Sash",
		legs  = "Crimson Cuisses",
		feet  = "Crimson Greaves"
	}

--	sets.idle.Town = {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

--	sets.idle.Weak = {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

	sets.resting = {
		main  = "Chatoyant Staff",
		sub   = "Bugard Leather Strap +1",
--		range = " ",
		ammo  = "Bibiki Seashell",
		head  = "Duelist's Chapeau +1",
		neck  = "Orochi Nodowa",
		ear1  = "Relaxing Earring",
		ring1 = "Star Ring",
		ear2  = "Star Earring",
		ring2 = "Star Ring",
		body  = "Dalmatica",
		hands = "Hydra Gloves",
		back  = "Hexerei Cape",
		waist = "Duelist's Belt",
		legs  = "Hydra Brais",
		feet  = "Avocat Pigaches",
	}

	sets.resting.Zenith = set_combine(sets.resting, sets.Zenith)

	------------------
	-- Engaged sets --
	------------------

	-- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
	-- sets if more refined versions aren't defined.
	-- If you create a set with both offense and defense modes, the offense mode should be first.
	-- EG: sets.engaged.Dagger.Accuracy.Evasion

	sets.engaged = {
		ammo  = "Bibiki Seashell",
		head  = "Walahra Turban",
		neck  = "Chivalrous Chain",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Ecphoria Ring",
		body  = "Assault Jerkin",
		hands = "Dusk Gloves +1",
		back  = "Umbra Cape",
		waist = "Velocious Belt",
		legs  = "Dusk Trousers",
		feet  = "Dusk Ledelsens +1",
	}

	sets.engaged.Haste = {
		ammo  = "Tiphia Sting",
		head  = "Walahra Turban",
		neck  = "Chivalrous Chain",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Ecphoria Ring",
		body  = "Assault Jerkin",
		hands = "Dusk Gloves +1",
		back  = "Cuchulain's Mantle",
		waist = "Velocious Belt",
		legs  = "Dusk Trousers",
		feet  = "Dusk Ledelsens +1",
	}

	sets.engaged.Accuracy = {
		ammo  = "Tiphia Sting",
		head  = "Optical Hat",
		neck  = "Chivalrous Chain",
		ear1  = "Brutal Earring",
		ring1 = "Lava's Ring",
		ear2  = "Suppanomimi",
		ring2 = "Kusha's Ring",
		body  = "Antares Harness",
		hands = "Hydra Gloves",
		back  = "Cuchulain's Mantle",
		waist = "Virtuoso Belt",
		legs  = "Hydra Brais",
		feet  = "Dusk Ledelsens +1",
	}

	-- This is mainly my RDM/NIN shadow soloing set I don't care about the -enmity.
	sets.engaged.Hydra = {
		ammo  = "Fenrir's Stone",
		head  = "Optical Hat",
		neck  = "Evasion Torque",
		ear1  = "Drone Earring",
		ring1 = "Breeze Ring",
		ear2  = "Suppanomimi",
		ring2 = "Breeze Ring",
		body  = "Hydra Doublet",
		hands = "Hydra Gloves",
		back  = "Boxer's Mantle",
		waist = "Scouter's Rope",
		legs  = "Hydra Brais",
		feet  = "Emissary Boots",
	}

	-- Combines the refresh hybrid mode gear with the different offense modes.
	sets.engaged.Refresh          = set_combine(sets.engaged, sets.Refresh)
	sets.engaged.Haste.Refresh    = set_combine(sets.engaged.Haste, sets.Refresh)
	sets.engaged.Accuracy.Refresh = set_combine(sets.engaged.Accuracy, sets.Refresh)
	sets.engaged.Hydra.Refresh    = set_combine(sets.engaged.Hydra, sets.Refresh)

	----------------
	-- Range sets --
	----------------

	sets.precast.RA = {
		range = "Lamian Kaman",
		ammo  = "Kabura Arrow",
	}

	-- But your Ranged attack enhancing gear here.
	sets.midcast.RA = {
		main  = "Terra's Staff",
		sub   = "Bugard Leather Strap +1",
		range = "Failnaught",
		ammo  = "Kabura Arrow",
		head  = "Optical Hat",
		neck  = "Peacock Amulet",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
		hands = "Crimson Finger Gauntlets",
--		back  = " ",
--		waist = " ",
		legs  = "Dusk Trousers",
--		feet  = " ",
	}

	-- Put your Ranged accuracy and attack enhancing gear here.
	sets.midcast.RA.Accuracy = set_combine(sets.midcast.RA, {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	})

	-----------------------
	-- Weapon skill sets --
	-----------------------

	sets.precast.WS = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Flame Ring",
		body  = "Assault Jerkin",
		hands = "Alkyoneus's Bracelets",
		back  = "Cerberus Mantle",
		waist = "Warwolf Belt",
		legs  = "Dusk Trousers",
		feet  = "Rutter Sabatons",
	}

	sets.precast.WS = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Flame Ring",
		body  = "Assault Jerkin",
		hands = "Alkyoneus's Bracelets",
		back  = "Cerberus Mantle",
		waist = "Warwolf Belt",
		legs  = "Dusk Trousers",
		feet  = "Rutter Sabatons",
	}

	sets.precast.WS.Accuracy = set_combine(sets.precast.WS, {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
		ring1 = "Lava's Ring",
--		ear2  = " ",
		ring2 = "Kusha's Ring",
--		body  = " ",
--		hands = " ",
		back  = "Cuchulain's Mantle",
		waist = "Virtuoso Belt",
--		legs  = " ",
--		feet  = " ",
	})

	--[[
	-- Scaling STR=30%?
	sets.precast.WS['Vorpal Blade'] = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Star Ring",
		body  = "Crimson Scale Mail",
		hands = "Alkyoneus's Bracelets",
		back  = "Prism Cape",
		waist = "Penitent's Rope",
		legs  = "Jet Seraweels",
		feet  = "Duelist's Boots +1",
	}

	-- Scaling STR=60%?
	sets.precast.WS['Requiescat'] = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Suppanomimi",
		ring2 = "Flame Ring",
		body  = "Assault Jerkin",
		hands = "Alkyoneus's Bracelets",
		back  = "Cerberus Mantle",
		waist = "Warwolf Belt",
		legs  = "Dusk Trousers",
		feet  = "Rutter Sabatons",
	}

	-- Scaling DEX=60% CHR=80%?
	sets.precast.WS['Exenterator'] = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Pixie Earring",
		ring2 = "Thunder Ring",
		body  = "Antares Harness",
		hands = "Warlock's Gloves",
		back  = "Cuchulain's Mantle",
		waist = "Warwolf Belt",
		legs  = "Oily Trousers",
		feet  = "Dancing Shoes +1",
	}

	-- Scaling STR=30% MND=50% code says phsyical WS not sure its supposed to be scaling seems to be missing.
	sets.precast.WS['Death Blossom'] = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Brutal Earring",
		ring1 = "Rajas Ring",
		ear2  = "Star Earring",
		ring2 = "Star Ring",
		body  = "Crimson Scale Mail",
		hands = "Alkyoneus's Bracelets",
		back  = "Prism Cape",
		waist = "Penitent's Rope",
		legs  = "Jet Seraweels",
		feet  = "Duelist's Boots +1",
	}

	-- Should be AGI=25% STR=16%?
	sets.precast.WS['Sidewinder'] = {
		ammo  = "Tiphia Sting",
		head  = "Gnadbhod's Helm",
		neck  = "Fotia Gorget",
		ear1  = "Drone Earring",
		ring1 = "Rajas Ring",
		ear2  = "Drone Earring",
		ring2 = "Breeze Ring",
		body  = "Antares Harness",
		hands = "Crimson Gloves",
		back  = "Cuchulain's Mantle",
		waist = "Warwolf Belt",
		legs  = "Oily Trousers",
		feet  = "Crimson Greaves",
	}
	--]]

	-----------------------
	-- Job ability sets. --
	-----------------------

--	sets.precast.JA.Convert = {
--		main  = " ",
--		sub   = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

--	sets.precast.Waltz = {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}
		
--	sets.precast.Waltz['Healing Waltz'] = {} -- Ensures that Healing Waltz doesn't swap gear in. It does not benefit from anything.

	------------------
	-- Midcast sets --
	------------------

		-------------------------
		-- Fastcast and Recast --
		-------------------------

	-- Set for Fastcast gear for spells used before every spell you cast.
	sets.precast.FC = {
		head = "Warlock's Chapeau",
		ear1 = "Loquacious Earring",
		body = "Duelist's Tabard +1",
	}

	-- Generic spell recast set (Haste > Conserve MP > Interrupt > Evasion > PDT) and the default casting set if you have none other.
	sets.midcast.FastRecast = {
		head  = "Walahra Turban",
--		body  = " ",
		hands = "Dusk Gloves +1",
		waist = "Velocious Belt",
--		legs  = " ",
		feet  = "Dusk Ledelsens +1",
	}

	sets.midcast.Utsusemi = sets.midcast.FastRecast -- To be honest not really needed but just in case.

		------------------------
		-- Healing and Curing --
		------------------------

	-- Healing skill gear set for spells that only scale on healing skill and inherits gear from the fast recast set.
	sets.midcast['Healing Magic'] = set_combine(sets.midcast.FastRecast, {
		body   = "Duelist's Tabard +1",
		legs   = "Warlock's Tights",
	})

	-- Main healing set for curing which inherits gear from the healing set which inherits from the fast recast set.
	sets.midcast.Cure = set_combine(sets.midcast['Healing Magic'], {
		main   = "Chatoyant Staff",
		sub    = "Raptor Leather Strap +1",
		neck   = "Faith Torque",
		ear1   = "Star Earring",
		ring1  = "Star Ring",
		ear2   = "Star Earring",
		ring2  = "Star Ring",
		back   = "Prism Cape",
	})

	sets.midcast.Curaga = sets.midcast.Cure -- Setting Curaga so it uses the Cure set.

	-- User configurable shitlist of healing spells that don't scale on anything that need to be directed back to the recast set.
	-- If I'm wrong about one delete it from this list, and if I'm missing any just add to the list.
	sets.midcast.Raise    = sets.midcast.FastRecast
	sets.midcast.Reraise  = sets.midcast.FastRecast
	sets.midcast.Blindna  = sets.midcast.FastRecast
	sets.midcast.Paralyna = sets.midcast.FastRecast
	sets.midcast.Poisona  = sets.midcast.FastRecast
	sets.midcast.Silena   = sets.midcast.FastRecast

		-------------------------
		-- Nuking and Helixes  --
		-------------------------

	-- Pure power set for nuking.
	sets.midcast['Elemental Magic'] = {
		main  = "Chatoyant Staff",
		sub   = "Bugard Strap +1",
		ammo  = "Phantom Tathlum",
		head  = "Warlock's Chapeau",
		neck  = "Uggalepih Pendant",
		ear1  = "Loquacious Earring",
		ring1 = "Snow Ring",
		ear2  = "Moldavite Earring",
		ring2 = "Snow Ring",
		body  = "Crimson Scale Mail",
		hands = "Zenith Mitts +1",
		back  = "Hecate's Cape",
		waist = "Penitent's Rope",
		legs  = "Jet Seraweels",
		feet  = "Duelist's Boots +1",
	}

	-- More accuracy less power nuking set combined with the base power nuking set.
	sets.midcast['Elemental Magic'].Resist = set_combine(sets.midcast['Elemental Magic'], {
		neck  = "Lemegeton Medallion +1",
		hands = "Errant Cuffs",
		back  = "Prism Cape",
		legs  = "Duelist's Tights +1",
	})

		----------------------------
		-- Enfeebling and Debuffs --
		----------------------------

	sets.midcast.ElementalEnfeeble = {}

	-- Potency based enfeebling set for white magic spells.
	sets.midcast.WhiteEnfeeble = {
		main  = "Chatoyant Staff",
		sub   = "Raptor Leather Strap +1",
		ammo  = "Sturm's Report",
		head  = "Duelist's Chapeau +1",
		neck  = "Faith Torque",
		ear1  = "Star Earring",
		ring1 = "Star Ring",
		ear2  = "Star Earring",
		ring2 = "Star Ring",
		body  = "Crimson Scale Mail",
		hands = "Bricta's Cuffs",
		back  = "Prism Cape",
		waist = "Penitent's Rope",
		legs  = "Jet Seraweels",
		feet  = "Duelist's Boots +1",
	}

	-- Accuracy based enfeebling set for White magic spells.
	sets.midcast.WhiteEnfeeble.Resist = set_combine(sets.midcast.WhiteEnfeeble, {
		body  = "Warlock's Tabard",
	})

	-- White magic enfeebles that should always be directed to the potency set.

	-- White magic enfeebles that should always be directed to the resist set.

	-- Potency based enfeebling set for black magic spells.
	sets.midcast.BlackEnfeeble = {
		main  = "Chatoyant Staff",
		sub   = "Bugard Leather Strap +1",
		ammo  = "Phantom Tathlum",
		head  = "Duelist's Chapeau +1",
		neck  = "Lemegeton Medallion +1",
		ear1  = "Loquacious Earring",
		ring1 = "Snow Ring",
		ear2  = "Morion Earring",
		ring2 = "Snow Ring",
		body  = "Crimson Scale Mail",
		hands = "Duelist's Gloves +1",
		back  = "Prism Cape",
		waist = "Penitent's Rope",
		legs  = "Jet Seraweels",
		feet  = "Avocat Pigaches",
	}

	-- Accuracy based enfeebling set for Black magic spells.
	sets.midcast.BlackEnfeeble.Resist = set_combine(sets.midcast.BlackEnfeeble, {
		body  = "Warlock's Tabard",
	})

	-- Black magic enfeebles that should always be directed to the potency set.

	-- Black magic enfeebles that should always be directed to the resist set.

		---------------------
		-- Dark and Divine --
		---------------------

	-- Dark magic skill gear set combined over your FastRecast set for spells that only scale on skill and maybe magic accuracy.
	sets.midcast['Dark Magic'] = set_combine(sets.midcast.FastRecast, {
		ammo  = "Sturm's Report",
		ear1  = "Loquacious Earring",
		hands = "Crimson Finger Gauntlets",
	})

	-- Divine magic skill gear set combined over your FastRecast set for spells that only scale on skill and maybe magic accuracy.
	sets.midcast['Divine Magic'] = set_combine(sets.midcast.FastRecast, {
		ammo  = "Sturm's Report",
		hands = "Bricta's Cuffs",
	})

		---------------------
		-- Enhancing Magic --
		---------------------

	-- Enhancing magic skill gear set combined over your FastRecast set for spells that only scale on skill.
	sets.midcast['Enhancing Magic'] = set_combine(sets.midcast.FastRecast, {
		hands = "Duelist's Gloves +1",
		legs  = "Warlock's Tights",
	})

	-- This may actually be the only enhancing spell that scales on any stat at all (MND) and it benefits from enhancing skill.
	sets.midcast.Stoneskin = set_combine(sets.midcast['Enhancing Magic'], {
		main  = "Chatoyant Staff",
		sub   = "Raptor Strap +1",
		head  = "Duelist's Chapeau +1",
		neck  = "Faith Torque",
		ear1  = "Star Earring",
		ring1 = "Star Ring",
		ear2  = "Star Earring",
		ring2 = "Star Ring",
		body  = "Crimson Scale Mail",
		back  = "Prism Cape",
		waist = "Penitent's Rope",
		feet  = "Duelist's Boots +1",
	})

	-- User configurable shitlist of enhancing spells that don't scale on anything that need to be directed back to the recast set.
	-- If I'm wrong about any of these just delete that one from the list, and if I'm missing any just add it to the list.
	sets.midcast.Aquaveil          = sets.midcast.FastRecast
	sets.midcast.Blink             = sets.midcast.FastRecast
	sets.midcast.Refresh           = sets.midcast.FastRecast
	sets.midcast.Haste             = sets.midcast.FastRecast
	sets.midcast.Flurry            = sets.midcast.FastRecast
	sets.midcast.Protect           = sets.midcast.FastRecast
	sets.midcast.Protectra         = sets.midcast.FastRecast
	sets.midcast.Shell             = sets.midcast.FastRecast
	sets.midcast.Shellra           = sets.midcast.FastRecast
	sets.midcast.Erase             = sets.midcast.FastRecast
	sets.midcast.Regen             = sets.midcast.FastRecast
	sets.midcast.Deodorize         = sets.midcast.FastRecast
	sets.midcast.Invisible         = sets.midcast.FastRecast
	sets.midcast.Sneak             = sets.midcast.FastRecast
	sets.midcast.Warp              = sets.midcast.FastRecast
	sets.midcast.Escape            = sets.midcast.FastRecast
	sets.midcast['Teleport-Dem']   = sets.midcast.FastRecast
	sets.midcast['Teleport-Mea']   = sets.midcast.FastRecast
	sets.midcast['Teleport-Holla'] = sets.midcast.FastRecast
	sets.midcast['Blaze Spikes']   = sets.midcast.FastRecast
	sets.midcast['Ice Spikes']     = sets.midcast.FastRecast
	sets.midcast['Shock Spikes']   = sets.midcast.FastRecast

	------------------
	-- Defense sets --
	------------------

	-- To use these you must set (//defense magical_or_physical_or_none) in conjuction with
	-- (//pdt set_name) or (//mdt set_name) -.- not a fan of how he did these.
	-- Tri-mode defense sets that all share a name space </3.

	sets.defense.PDT = {
		ammo  = "Bibiki Seashell",
--		ring2 = "Defending Ring",
		back  = "Umbra Cape",
	}

	sets.defense.MDT = {
--		ring2 = "Defending Ring",
		back  = "Hexerei Cape",
	}

	sets.Kiting = {legs = "Crimson Cuisses"} -- Alt+F10 to toggle pinning on and off.

	--------------
	-- Pin sets --
	--------------

	--[[
	These sets can be quick pinned with //pin set_name_here
	They can also be unpinned with //pin none
	But only one set can be pinned at a time that way.

	These sets can also be called and pinned in a windower macro using the example script.txt below.
	gs enable neck
	gs equip manual.whistle
	gs disable neck

	Or used (and pinned) in a standard in game macro with the example below.
	/console gs enable neck
	/console equip pin.whistle
	/console gs disable neck

	Make sure you enable then equip and then disable all the slots that are used in the sets.
	--]]

	-- If you pin a main hand, pin a sub hand as well, if you pin a ring pin two instead rings.

	sets.pin.Whistle = {neck  = "Chocobo Whistle"}
	sets.pin.Warp    = {ring2 = "Warp Ring"}
	sets.pin.Nexus   = {back  = "Nexus Cape"}
	sets.pin.Lotus   = {
		main  = "Lotus Katana",
		sub   = "Bugard Leather Strap +1", -- Code quirk without this it can bug your sub slot out when swapping back.
	}

	-- Pin set I use while casting when I believe I'm going to be hit a lot.
	sets.pin.Terra = {
		main  = "Terra's Staff",
		ammo  = "Bibiki Seashell",
		sub   = "Bugard Leather Strap +1",
		back  = "Umbra Cape",
	}

	-- Adding a few universal sets to the pin sets.
	sets.pin.Refresh = sets.Refresh
	sets.pin.Zenith  = sets.Zenith

	----------------------
	-- Custom buff sets --
	----------------------

--	sets.buff.Sleep = {neck = "Opo-opo Necklace"} -- Needs testing.

end

-----------------------------------------------------
-- Job-specific hooks for standard casting events. --
-----------------------------------------------------

-- Set eventArgs.handled to true if we don't want any automatic target handling to be done.
function job_pretarget(spell, action, spellMap, eventArgs)

end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
-- Set eventArgs.useMidcastGear to true if we want midcast gear equipped on precast.
function job_precast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default precast() is done.
-- eventArgs is the same one used in job_precast, in case information needs to be persisted.
function job_post_precast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)
	-- Staffs the types of magic this applies to is ghetto listed because that is easier to control and modify this is a configuration file first.
	if state.Staff.value == true then
		if spell.action_type == "Magic" and
		   spell.skill == 'Elemental Magic' or
		   spell.skill == "Enfeebling Magic" or
		   spell.skill == "Healing Magic" or
		   spell.skill == 'Dark Magic' or
		   spell.skill == "Divine Magic" then
			equip(sets.staff[spell.element])
		end
	else
		if spell.action_type == "Magic" and
		   spell.skill == 'Elemental Magic' and
		   spell.element == 'Ice' and
		   IceStaff == true then
			equip(sets.staff['Ice'])
		end
	end

	-- Grips the types of magic this applies to is ghetto listed because that is easier to control and modify this is a configuration file first.
	if state.Grip.value == true then
		if spell.action_type == "Magic" and
		   spell.skill == 'Elemental Magic' or
		   spell.skill == "Enfeebling Magic" or
		   spell.skill == 'Dark Magic' or
		   spell.skill == "Divine Magic" then
			equip(sets.grips[spell.element])
		end
	end

	-- Obis the types of magic this applies to is ghetto listed because that is easier to control and modify this is a configuration file first.
	if state.Obi.value == true then
		if spell.action_type == "Magic" and
		   spell.skill == 'Elemental Magic' or
		   spell.skill == 'Healing Magic' or
		   spell.skill == 'Dark Magic' or
		   spell.skill == 'Divine Magic' then
			if spell.element == world.weather_element or spell.element == world.day_element then
				if MasterObi == true then
					equip(sets.obi['Master'])
				else
					equip(sets.obi[spell.element])
				end
			end
		end
	end

	cast_override_slots()
end

-- Runs when a pet initiates an action.
-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_pet_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default pet midcast() is done.
-- eventArgs is the same one used in job_pet_midcast, in case information needs to be persisted.
function job_pet_post_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default aftercast() is done.
-- eventArgs is the same one used in job_aftercast, in case information needs to be persisted.
function job_post_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_pet_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default pet aftercast() is done.
-- eventArgs is the same one used in job_pet_aftercast, in case information needs to be persisted.
function job_pet_post_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Function to overide certain slots like weapons in different job event fuctions under different conditions.
function cast_override_slots()
	if state.SaveTP.value == true then
		equip(sets.ammo[state.Ammo.current])
		if dualwield == false then
			equip(sets.weapon[state.Weapon.current])
		else
			equip(sets.weapons[state.Weapons.current])
		end
	end
	equip(sets.pin[state.Pin.current])
end

------------------------------------------------
-- Job-specific hooks for non-casting events. --
------------------------------------------------

--[[ This is kicking my ass.
-- Called when the player's status changes.
function job_status_change(newStatus, oldStatus, eventArgs)
	eventArgs.handled = true

	if newStatus == 'Idle' then
		equip(sets.idle[state.IdleMode.current])
	elseif newStatus == 'Engaged' then
		equip(sets.engaged[state.OffenseMode.current])
	elseif newStatus == 'Resting' then
		equip(override_slots(override_weapons(sets.resting[state.RestingMode.current])))
	end

end
--]]

-- Called when the player's status changes.
function job_status_change(newStatus, oldStatus, eventArgs)

end

-- Called when the player's pet's status changes.
function job_pet_status_change(newStatus, oldStatus, eventArgs)

end

-- Called when a player gains or loses a buff.
-- buff == buff gained or lost
-- gain == true if the buff was gained, false if it was lost.
function job_buff_change(buff, gain)

end

-- Called when a generally-handled state value has been changed.
function job_state_change(stateField, newValue, oldValue)

end

------------------------------------------------------------
-- User code that supplements standard library decisions. --
------------------------------------------------------------

-- Called before the Include starts constructing melee/idle/resting sets.
-- Can customize state or custom melee class values at this point.
-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_handle_equipping_gear(status, eventArgs)

end

-- Custom spell mapping.
-- Return custom spellMap value that can override the default spell mapping.
-- Don't return anything to allow default spell mapping to be used.
function job_get_spell_map(spell, default_spell_map)
	if spell.action_type == 'Magic' then
		if spell.skill == 'Enfeebling Magic' then
			if spell.type == 'WhiteMagic' then
				return 'WhiteEnfeeble'
			else
				return 'BlackEnfeeble'
			end
		end
	end
end

-- Return a customized weaponskill mode to use for weaponskill sets.
-- Don't return anything if you're not overriding the default value.
function get_custom_wsmode(spell, spellMap, default_wsmode)

end

-- Modify the default idle set after it was constructed.
function customize_idle_set(idleSet)
	if state.SaveTP.value == true then
		idleSet = override_slots(override_weapons(idleSet))
	else
		idleSet = override_slots(idleSet)
	end

	return idleSet
end

-- Modify the default melee set after it was constructed.
function customize_melee_set(meleeSet)
	meleeSet = override_slots(override_weapons(meleeSet))

	return meleeSet
end

-- Modify the default defense set after it was constructed.
function customize_defense_set(defenseSet)
	defenseSet = override_slots(override_weapons(defenseSet))

	return defenseSet
end

-- Called by the 'update' self-command, for common needs.
-- Set eventArgs.handled to true if we don't want automatic equipping of gear.
function job_update(cmdParams, eventArgs)

end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)

end

-- Separated from the general slot override part contains code to override weapons, ranged, and ammo.
function override_weapons(Set)
	if dualwield == false then
		Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapon[state.Weapon.current])
	else
		Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapons[state.Weapons.current])
	end

	return Set
end

-- Separated from the weapon override part contains code to override everything but weapons, ranged, and ammo.
-- !!!This separation could be the cause of the pin set issues!!! investigate later when you have time.
function override_slots(Set)
	Set = set_combine(Set, sets.pin[state.Pin.current])

	return Set
end

-----------------------------------------------
-- User code that supplements self-commands. --
-----------------------------------------------

-- Called for custom player commands.
function job_self_command(cmdParams, eventArgs)

end

-- Job-specific toggles.
function job_toggle_state(field)

end

-- Request job-specific mode lists.
-- Return the list, and the current value for the requested field.
function job_get_option_modes(field)

end

-- Set job-specific mode values.
-- Return true if we recognize and set the requested field.
function job_set_option_mode(field, val)

end

-- Handle auto-targetting based on local setup.
function job_auto_change_target(spell, action, spellMap, eventArgs)

end

---------------------------------------------
-- Utility functions specific to this job. --
---------------------------------------------

-- Checks if you can dual wield or not and sets the correct default weapon set.
function dual_wield_check()
	if player.job ~= "NIN" or player.job ~= "DNC" then
		if player.sub_job == "NIN" and player.sub_job_level >= 10 or
		   player.sub_job == "DNC" and player.sub_job_level >= 20 then
			dualwield = true
		else
			dualwield = false
		end
	else
		dualwield = true
	end
end

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
	-- Default macro set/book
		set_macro_page(1, 5)
end
