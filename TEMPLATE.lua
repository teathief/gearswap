----------------------------------------------------------------------
-- Setup functions for this job.  Generally should not be modified. --
----------------------------------------------------------------------

-- Initialization function for this job file.
function get_sets()
	mote_include_version = 2

	-- Load and initialize the include file.
	include('Mote-Include.lua')
end

-- Setup vars that are user-independent.  state.Buff vars initialized here will automatically be tracked.
function job_setup()

end

-----------------------------------------------------------------------------------------------
-- User setup functions for this job.  Recommend that these be overridden in a sidecar file. --
-----------------------------------------------------------------------------------------------

-- Setup vars that are user-dependent.  Can override this function in a sidecar file.
function user_setup()
	-- Your single wielding weapon sets.
	state.Weapon = M ('Single')

	-- Your dual wielding weapon sets.
	state.Weapons = M ('Dual')

	-- Your ranged/ammo weapon sets and set to 'None' if you want no ranged weapons sets during //savetp.
	state.Ammo = M ('None', 'Normal')

	-- F9 cycles thru offense modes.
	state.OffenseMode:options('Normal', 'Accuracy')

	-- Alt+F9 cycles ranged modes.
	state.RangedMode:options('Normal', 'Accuracy')

	-- Ctrl+F9 cycles hybrid modes.
	state.HybridMode:options('None')

	-- Doesn't have a default keybind to cycle modes.
	state.WeaponskillMode:options('Normal', 'Accuracy')

	-- Ctrl+F11 cycles casting modes.
	state.CastingMode:options('Normal')

	-- Ctrl+F12 cycles idle modes.
	state.IdleMode:options('Normal')

	-- Doesn't have a default keybind to cycle modes.
	state.RestingMode:options('Normal')

	-- F10 to turn on PDT to your current PDT mode and Ctrl+F10 to turn off.
	state.PhysicalDefenseMode:options('PDT', 'None')

	-- F11 to turn on MDT to your current MDT mode and Ctrl+F10 to turn off.
	state.MagicalDefenseMode:options('MDT', 'None')

	-- Quick pin sets for the quick pin command.
	state.Pin = M ('None', 'Whistle', 'Warp', 'Nexus', 'Lotus')

	-- Set true to false if you want to begin without TP saving on.
	state.SaveTP = M (true, 'Saving')

	-- Aliases to control modes which I find much easier to use then remembering all those damn function keybinds.
	send_command('alias weapon gs c set Weapon')           -- //weapon set.name.here to change single weapon sets.
	send_command('alias weapons gs c set Weapons')         -- //weapons set.name.here to change dual weapon sets.
	send_command('alias ammo gs c set Ammo')               -- //ammo set.name.here to change default ranged and ammo weapon sets.
	send_command('alias tp gs c set OffenseMode')          -- //tp set.name.here to change engaged sets.
	send_command('alias ranged gs c set RangedMode')       -- //range set.name.here to change ranged sets.
	send_command('alias hybrid gs c set HybridMode')       -- //hybrid set.name.here to change hybrid sets.
	send_command('alias ws gs c set WeaponskillMode')      -- //ws set.name.here to change ws mode sets.
	send_command('alias cast gs c set CastingMode')        -- //cast set.name.here to change casting mode sets.
	send_command('alias idle gs c set IdleMode')           -- //idle set.name.here to change idle mode sets.
	send_command('alias rest gs c set RestingMode')        -- //rest set.name.here to change resting mode sets.
	send_command('alias defense gs c set DefenseMode')     -- //defense 'physical' or 'magical' or 'none' may be required to even use PDT and MDT sets.
	send_command('alias pdt gs c set PhysicalDefenseMode') -- //pdt set.name.here to change PDT mode sets.
	send_command('alias mdt gs c set MagicalDefenseMode')  -- //mdt set.name.here to change MDT mode sets.
	send_command('alias kiting gs c toggle Kiting')        -- //kiting to toggle pinning the kiting set on or off.
	send_command('alias equip gs equip')                   -- //equip set.name.here to force equip a set but it does not lock it in place.
	send_command('alias pin gs c set Pin')                 -- //pin to quick equip and pin a set.
	send_command('alias savetp gs c toggle savetp')        -- //savetp to toggle your savetp state.

	send_command('gs enable all')                   -- Unlock any slots that may have been locked before this gearswap was loaded.
	send_command('wait 10; input /lockstyleset 01') -- Sets your lockstyle set to the two digit number at the end.
	select_default_macro_book()                     -- Check the function at the bottom of this file to set the correct book and page number.

	send_command('bind %^space gs disable main sub range') -- Press ctrl + space to lock weapons.
	send_command('bind %!space gs enable main sub range')  -- Press alt + space to unlock weapons.

	dual_wield_check() -- Checks if you can dual wield and sets the var dualwield to true or false.

end

-- Called when this job file is unloaded (eg: job change)
function user_unload()
	-- Remove the bindings set above when this gearswap is unloaded.
	send_command('unbind %^space')
	send_command('unbind %!space')

--	send_command('clearaliases') -- That I know of there is no command to clear individual aliases and I don't want to stomp on anything.

end

---------------
-- Gear sets --
---------------

function init_gear_sets()

	sets.weapon  = {} -- Creates a empty list for single weapon sets to belong to.
	sets.weapons = {} -- Creates a empty list for dual weapon sets to belong to.
	sets.ammo    = {} -- Creates a emtpy list for ranged weapon sets to belong to.
	sets.pin     = {} -- Creates a empty list for pin sets to belong to.

	-----------------
	-- Global sets --
	-----------------

	-----------------
	-- Weapon sets --
	-----------------

	sets.weapon.Single = {
		main = " ",
		sub  = " ",
	}

	sets.weapons.Dual = {
		main = " ",
		sub  = " ",
	}

	---------------------------------
	-- Ranged weapon and ammo sets --
	---------------------------------

	-- Just put your range and ammo pieces in here.
	sets.ammo.Normal = {
		range = " ",
		ammo  = " ",
	}

	---------------
	-- Idle sets --
	---------------

	sets.idle = {
		main  = " ",
		sub   = " ",
		range = " ",
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}

--	sets.idle.Town = {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

--	sets.idle.Weak = {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}

	sets.resting = {
		main  = " ",
		sub   = " ",
		range = " ",
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}
	
	------------------
	-- Engaged sets --
	------------------

	-- Variations for TP weapon and (optional) offense/defense modes.  Code will fall back on previous
	-- sets if more refined versions aren't defined.
	-- If you create a set with both offense and defense modes, the offense mode should be first.
	-- EG: sets.engaged.Dagger.Accuracy.Evasion
	
	sets.engaged = {
		main  = " ",
		sub   = " ",
		range = " ",
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}

	sets.engaged.Accuracy = set_combine(sets.engaged, {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	})

	----------------
	-- Range sets --
	----------------

	-- This might benefit from all your haste gear not on Era though.
	sets.precast.RA = {
--		main = " ",
--		sub  = " ",
	}

	-- Put your Ranged attack enhancing gear here.
	sets.midcast.RA = {
		main  = " ",
		sub   = " ",
		range = " ",
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}

	-- But your Ranged accuracy and attack enhancing gear here.
	sets.midcast.RA.Accuracy = set_combine(sets.midcast.RA, {
--		main  = " ",
--		sub   = " ",
--		range = " ",
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	})

	-----------------------
	-- Weapon skill sets --
	-----------------------

	-- Corresponds to 'Normal' in Weaponskill modes.
	sets.precast.WS = {
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}

	sets.precast.WS.Accuracy = set_combine(sets.precast.WS, {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
	})

	-----------------------
	-- Job ability sets. --
	-----------------------

--	sets.precast.Waltz = {
--		ammo  = " ",
--		head  = " ",
--		neck  = " ",
--		ear1  = " ",
--		ring1 = " ",
--		ear2  = " ",
--		ring2 = " ",
--		body  = " ",
--		hands = " ",
--		back  = " ",
--		waist = " ",
--		legs  = " ",
--		feet  = " ",
--	}
		
--	sets.precast.Waltz['Healing Waltz'] = {} -- Ensures that Healing Waltz doesn't swap gear in. It does not benefit from anything.

	------------------
	-- Midcast sets --
	------------------

	sets.precast.FC = {ear2="Loquacious Earring"} -- Sets for fast cast gear for spells

	-- Generic spell recast set (Haste > Interrupt > Evasion > PDT) and the default casting set if you have none other.
	sets.midcast.FastRecast = {
		main  = " ",
		sub   = " ",
		range = " ",
		ammo  = " ",
		head  = " ",
		neck  = " ",
		ear1  = " ",
		ring1 = " ",
		ear2  = " ",
		ring2 = " ",
		body  = " ",
		hands = " ",
		back  = " ",
		waist = " ",
		legs  = " ",
		feet  = " ",
	}

	sets.midcast.Utsusemi = sets.midcast.FastRecast -- To be honest not really needed but just in case.

	------------------
	-- Defense sets --
	------------------

	-- To use these you must set (//defense magical_or_physical) in conjuction with
	-- (//pdt set_name) or (//mdt set_name) -.- not a fan of how he did these.
	-- Tri-mode defense sets that all share a name space </3.

	sets.defense.PDT = {}

	sets.defense.MDT = {}

	sets.Kiting = {} -- Alt+F10 to toggle pinning on and off.

	--------------
	-- Pin sets --
	--------------

	--[[
	These sets can be quick pinned with //pin set_name_here
	They can also be unpinned with //pin none
	But only one set can be pinned at a time that way.

	These sets can also be called and pinned in a windower macro using the example script.txt below.
	gs enable neck
	gs equip manual.whistle
	gs disable neck

	Or used (and pinned) in a standard in game macro with the example below.
	/console gs enable neck
	/console equip pin.whistle
	/console gs disable neck

	Make sure you enable then equip and then disable all the slots that are used in the sets.
	--]]

	sets.pin.Whistle = {neck  = "Chocobo Whistle"}
	sets.pin.Warp    = {ring2 = "Warp Ring"}
	sets.pin.Nexus   = {back  = "Nexus Cape"}
	sets.pin.Lotus   = {
		main = "Lotus Katana"
--		sub  = " ", -- Code quirk without this it can bug your sub slot out when swapping back.
	}

	----------------------
	-- Custom buff sets --
	----------------------

	sets.buff.Sleep = {neck = "Opo-opo Necklace"} -- Needs testing.

end

-----------------------------------------------------
-- Job-specific hooks for standard casting events. --
-----------------------------------------------------

-- Set eventArgs.handled to true if we don't want any automatic target handling to be done.
function job_pretarget(spell, action, spellMap, eventArgs)

end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
-- Set eventArgs.useMidcastGear to true if we want midcast gear equipped on precast.
function job_precast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default precast() is done.
-- eventArgs is the same one used in job_precast, in case information needs to be persisted.
function job_post_precast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default midcast() is done.
-- eventArgs is the same one used in job_midcast, in case information needs to be persisted.
function job_post_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Runs when a pet initiates an action.
-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_pet_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default pet midcast() is done.
-- eventArgs is the same one used in job_pet_midcast, in case information needs to be persisted.
function job_pet_post_midcast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default aftercast() is done.
-- eventArgs is the same one used in job_aftercast, in case information needs to be persisted.
function job_post_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_pet_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Run after the default pet aftercast() is done.
-- eventArgs is the same one used in job_pet_aftercast, in case information needs to be persisted.
function job_pet_post_aftercast(spell, action, spellMap, eventArgs)

	cast_override_slots()
end

-- Function to overide certain slots like weapons in different job event fuctions under different conditions.
function cast_override_slots()
	if state.SaveTP.value == true then
		equip(sets.ammo[state.Ammo.current])
		if dualwield == false then
			equip(sets.weapon[state.Weapon.current])
		else
			equip(sets.weapons[state.Weapons.current])
		end
	end
	equip(sets.pin[state.Pin.current])
end

------------------------------------------------
-- Job-specific hooks for non-casting events. --
------------------------------------------------

-- Called when the player's status changes.
function job_status_change(newStatus, oldStatus, eventArgs)

end

-- Called when the player's pet's status changes.
function job_pet_status_change(newStatus, oldStatus, eventArgs)

end

-- Called when a player gains or loses a buff.
-- buff == buff gained or lost
-- gain == true if the buff was gained, false if it was lost.
function job_buff_change(buff, gain)

end

-- Called when a generally-handled state value has been changed.
function job_state_change(stateField, newValue, oldValue)

end

------------------------------------------------------------
-- User code that supplements standard library decisions. --
------------------------------------------------------------

-- Called before the Include starts constructing melee/idle/resting sets.
-- Can customize state or custom melee class values at this point.
-- Set eventArgs.handled to true if we don't want any automatic gear equipping to be done.
function job_handle_equipping_gear(status, eventArgs)

end

-- Custom spell mapping.
-- Return custom spellMap value that can override the default spell mapping.
-- Don't return anything to allow default spell mapping to be used.
function job_get_spell_map(spell, default_spell_map)

end

-- Return a customized weaponskill mode to use for weaponskill sets.
-- Don't return anything if you're not overriding the default value.
function get_custom_wsmode(spell, spellMap, default_wsmode)

end

-- Modify the default idle set after it was constructed.
function customize_idle_set(idleSet)
	if state.SaveTP.value == true then
		idleSet = override_slots(override_weapons(idleSet))
	else
		idleSet = customize_override_slots(idleSet)
	end

	return idleSet
end

-- Modify the default melee set after it was constructed.
function customize_melee_set(meleeSet)
	meleeSet = override_slots(override_weapons(meleeSet))

	return meleeSet
end

-- Modify the default defense set after it was constructed.
function customize_defense_set(defenseSet)
	defenseSet = override_slots(override_weapons(defenseSet))

	return defenseSet
end

-- Called by the 'update' self-command, for common needs.
-- Set eventArgs.handled to true if we don't want automatic equipping of gear.
function job_update(cmdParams, eventArgs)

end

-- Set eventArgs.handled to true if we don't want the automatic display to be run.
function display_current_job_state(eventArgs)

end

-- Separated from the general slot override part contains code to override weapons, ranged, and ammo.
function override_weapons(Set)
	if dualwield == false then
		Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapon[state.Weapon.current])
	else
		Set = set_combine(Set, sets.ammo[state.Ammo.current], sets.weapons[state.Weapons.current])
	end

	return Set
end

-- Separated from the weapon override part contains code to override everything but weapons, ranged, and ammo.
function override_slots(Set)
	Set = set_combine(Set, sets.pin[state.Pin.current])

	return Set
end

-----------------------------------------------
-- User code that supplements self-commands. --
-----------------------------------------------

-- Called for custom player commands.
function job_self_command(cmdParams, eventArgs)

end

-- Job-specific toggles.
function job_toggle_state(field)

end

-- Request job-specific mode lists.
-- Return the list, and the current value for the requested field.
function job_get_option_modes(field)

end

-- Set job-specific mode values.
-- Return true if we recognize and set the requested field.
function job_set_option_mode(field, val)

end

-- Handle auto-targetting based on local setup.
function job_auto_change_target(spell, action, spellMap, eventArgs)

end

---------------------------------------------
-- Utility functions specific to this job. --
---------------------------------------------

-- Checks if you can dual wield or not and sets the correct default weapon set.
function dual_wield_check()
	if player.job ~= "NIN" or player.job ~= "DNC" then
		if player.sub_job == "NIN" and player.sub_job_level >= 10 or
		   player.sub_job == "DNC" and player.sub_job_level >= 20 then
			dualwield = true
		else
			dualwield = false
		end
	else
		dualwield = true
	end
end

-- Select default macro book on initial load or subjob change.
function select_default_macro_book()
	-- Default macro set/book
		set_macro_page(1, 1)
end
